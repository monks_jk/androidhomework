package com.kkrasylnykov.l11_logsandmultiactivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    public final static String KEY_NAME = "KEY_NAME";
    public static final String RESULT_KEY_DATA = "RESULT_KEY_DATA";

    public static final int RESULT_NO = 3;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_second);
        String strName = "";

        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                strName = bundle.getString(KEY_NAME, "--EMPTY--");
            }
        }

        RadioButton redRadioButton = (RadioButton)findViewById(R.id.radioButtonRed);
        redRadioButton.setOnClickListener(this);

        RadioButton greenRadioButton = (RadioButton)findViewById(R.id.radioButtonBlue);
        greenRadioButton.setOnClickListener(this);

        RadioButton blueRadioButton = (RadioButton)findViewById(R.id.radioButtonGreen);
        blueRadioButton.setOnClickListener(this);
        View btnMoveToSecondActivity = findViewById(R.id.buttonGetResultSesondActivity);
        btnMoveToSecondActivity.setOnClickListener(this);

    }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            switch (v.getId()) {
                case R.id.radioButtonRed:
                    intent.putExtra(RESULT_KEY_DATA+"1","Darth Vader");
                    setResult(RESULT_OK, intent);
                    break;
                case R.id.radioButtonBlue:
                    intent.putExtra(RESULT_KEY_DATA+"1","Darth Sidious");
                    setResult(RESULT_OK, intent);
                    break;
                case R.id.radioButtonGreen:
                    intent.putExtra(RESULT_KEY_DATA+"1","Master Yoda");
                    setResult(RESULT_OK, intent);
                    break;
                case R.id.buttonGetResultSesondActivity:
                    finish();
                    break;
                default:
                    setResult(RESULT_NO);
                    break;
            }
        }
}

