package com.kkrasylnykov.l11_logsandmultiactivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static String strName;
    private static final int REQUEST_CODE_SECOND_ACTIVITY = 101;
    private static final int REQUEST_CODE_RESULT_ACTIVITY = 102;

    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View btnMoveToSecondActivity = findViewById(R.id.butttonMoveToSecondAdctivityMainActivity);
        btnMoveToSecondActivity.setOnClickListener(this);

        editText = (EditText) findViewById(R.id.editTextNameMainActivity);
    }

    @Override
    public void onClick(View view) {
        Log.d("devcpp", "MainActivity -> onClick -> " + view.getId());
        switch (view.getId()){
            case R.id.butttonMoveToSecondAdctivityMainActivity:
                strName = editText.getText().toString();
                if ("".equals(strName)){
                    Toast.makeText(this, "Name is empty!!!!", Toast.LENGTH_LONG).show();
                    return;
                }
                Intent intent = new Intent(this,SecondActivity.class);
                intent.putExtra(SecondActivity.KEY_NAME,strName);
                startActivityForResult(intent, REQUEST_CODE_SECOND_ACTIVITY);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==REQUEST_CODE_SECOND_ACTIVITY){
            if (resultCode==RESULT_OK && data!=null){
                Bundle bundle = data.getExtras();
                if (bundle!=null){
                    Intent intent = new Intent(this,ResultActivity.class);
                    intent.putExtra(ResultActivity.KEY_RESULT,bundle.getString(SecondActivity.RESULT_KEY_DATA+"1"));
                    startActivityForResult(intent, REQUEST_CODE_RESULT_ACTIVITY);
                }
            }else{
                editText.getText().clear();
            }
        }
    }
}
