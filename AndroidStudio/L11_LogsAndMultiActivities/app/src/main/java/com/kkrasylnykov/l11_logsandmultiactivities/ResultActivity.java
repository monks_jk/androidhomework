package com.kkrasylnykov.l11_logsandmultiactivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    public final static String KEY_RESULT = "KEY_RESULT";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d("devcpp", "RESULT CREATE");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Intent intent = getIntent();
        TextView textView = (TextView) findViewById(R.id.textViewResultActivity);
        String strName = "";
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                strName = bundle.getString(KEY_RESULT);
            }
        }
        textView.setText("Hello, " + MainActivity.strName + "! "+ "Your new name is: " + strName);
        ImageView img = (ImageView)findViewById(R.id.imageViewResultActivity);
        String vader = "Darth Vader", sidious = "Darth Sidious", yoda = "Master Yoda";
        if (vader.equals(strName)) {
            img.setImageResource(R.drawable.veider);
        }else if(sidious.equals(strName)){
            img.setImageResource(R.drawable.sidious);
        }else if(yoda.equals(strName)){
            img.setImageResource(R.drawable.yoda);
        }
    }
}
