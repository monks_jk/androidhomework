package com.monks.memento_mori.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.monks.memento_mori.R;
import com.monks.memento_mori.fragment.GetTimeFragment;
import com.monks.memento_mori.fragment.InfoFragment;
import com.monks.memento_mori.fragment.MainFragment;
import com.monks.memento_mori.fragment.ShareTimeFragment;
import com.monks.memento_mori.fragment.StatisticsFragment;
import com.monks.memento_mori.service.TimeHeartbeatsManagerService;
import com.monks.memento_mori.tools.AppSettings;
import com.monks.memento_mori.tools.SetTimeViewWithDelay;
import com.monks.memento_mori.tools.TimerCountDown;

import java.util.Timer;

public class MainActivity extends AppCompatActivity implements MainFragment.OnSelectedButtonListener, View.OnClickListener {
    private static final int REQUEST_NAME = 101;
    public static  final int SHARE_TIME = 1;
    public static  final int GET_TIME = 2;
    public static  final int STATISTICS = 3;
    public static  final int INFO = 4;
    public static  final int HISTORY = 5;

    private AppSettings appSettings;
    public  TextView timeView;

    private FragmentManager manager = getSupportFragmentManager();
    private ShareTimeFragment shareTimeFragment = new ShareTimeFragment();
    private GetTimeFragment getTimeFragment = new GetTimeFragment();
    private StatisticsFragment statisticsFragment = new StatisticsFragment();
    private InfoFragment infoFragment = new InfoFragment();
    private MainFragment mainFragment = new MainFragment();
    private SetTimeViewWithDelay task;
    private Timer timer;

    private TimerCountDown timerCountDown = TimerCountDown.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appSettings = new AppSettings(this);
        timeView = (TextView)findViewById(R.id.timeViewMainActivity);

        if(appSettings.ifFirstRun()){
            startService(new Intent(this, TimeHeartbeatsManagerService.class));
            Intent intent = new Intent(this, FirstRunNameActivity.class);
            startActivityForResult(intent, REQUEST_NAME);
        }else {
            if (appSettings.getGameStatus()) {
                startService(new Intent(this, TimeHeartbeatsManagerService.class));
            }else{
                Button btnRefresh = (Button) findViewById(R.id.buttonRefreshMainActivity);
                Button btnRefreshAll = (Button) findViewById(R.id.buttonRefreshAllMainActivity);
                btnRefresh.setClickable(true);
                btnRefresh.setVisibility(View.VISIBLE);
                btnRefresh.setOnClickListener(this);
                btnRefreshAll.setClickable(true);
                btnRefreshAll.setVisibility(View.VISIBLE);
                btnRefreshAll.setOnClickListener(this);
            }
        }
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment, mainFragment);
        transaction.commit();
    }

    @Override
    protected void onResume() {
        if(appSettings.getGameStatus()){
            calculateWorkTime();
            appSettings.setCurrentTimeOn(System.currentTimeMillis() / 1000);
            timer = new Timer();
            task = new SetTimeViewWithDelay(this, timeView, timerCountDown);
            timer.scheduleAtFixedRate(task,TimerCountDown.DELAY,TimerCountDown.PERIOD);
            timerCountDown.startTimer(appSettings.getTime());
            timerCountDown.startTimerHeartbeats(appSettings.getHeartbeats());
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        if(appSettings.getGameStatus() && timer != null) {
            timer.cancel();
            timerCountDown.countDownTimer.cancel();
            timerCountDown.countDownTimerHeartbeats.cancel();
        }
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_NAME && resultCode == RESULT_CANCELED){
            this.finish();
        }
        if(requestCode == REQUEST_NAME && resultCode == RESULT_OK){
            appSettings.setShowTerms(false);
        }
    }

    @Override
    public void onButtonSelected(int buttonIndex) {
        Intent intent;
        switch (buttonIndex) {
            case SHARE_TIME:
                changeFragment(shareTimeFragment);
                break;
            case GET_TIME:
                changeFragment(getTimeFragment);
                break;
            case STATISTICS:
                changeFragment(statisticsFragment);
                break;
            case INFO:
                changeFragment(infoFragment);
                break;
            case HISTORY:
                intent = new Intent(this, HistoryActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.alfa_exit, R.anim.alfa_enter);
                break;
        }
    }

    private void changeFragment(Fragment fragment){
        FragmentTransaction transaction;
        transaction = manager.beginTransaction();
        transaction.remove(infoFragment).remove(getTimeFragment).remove(shareTimeFragment).remove(statisticsFragment);
        transaction.commit();
        transaction = manager.beginTransaction();
        transaction.setCustomAnimations(R.anim.alfa_exit, R.anim.alfa_enter,
                R.anim.alfa_exit, R.anim.alfa_enter);
        transaction.hide(mainFragment);
        transaction.add(R.id.fragment, fragment).addToBackStack(null);
        transaction.commit();
    }

    private void calculateWorkTime(){
        appSettings.setCurrentTimeOff(System.currentTimeMillis() / 1000);
        long currentTimeOff = appSettings.getCurrentTimeOff();
        long currentTimeOn = appSettings.getCurrentTimeOn();
        long dissTime = appSettings.getTime() - (currentTimeOff - currentTimeOn);
        long dissTimeStatistic = currentTimeOff - currentTimeOn;
        if(dissTime > 0){
            appSettings.setTime(dissTime);
            long statisticTimeToday = appSettings.getStatisticTimeToday();
            long statisticTimeAllTime = appSettings.getStatisticTimeAllTime();

            appSettings.setStatisticTimeToday(statisticTimeToday + dissTimeStatistic);
            appSettings.setStatisticTimeWeek(dissTimeStatistic);
            appSettings.setStatisticTimeAllTime(statisticTimeAllTime + dissTimeStatistic);
        }
        long dissHeart = appSettings.getHeartbeats() - (currentTimeOff - currentTimeOn);
        if(dissHeart > 0){
            appSettings.setHeartbeats(dissHeart);
        }
    }

    @Override
    public void onClick(View v) {
        AppSettings appSettings = new AppSettings(this);
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        switch (v.getId()) {
            case R.id.buttonRefreshMainActivity:
                appSettings.setTime(AppSettings.TIME_FOR_DAY);
                appSettings.setHeartbeats(AppSettings.HEARTBEATS);
                appSettings.setCurrentTimeOff(0);
                appSettings.setCurrentTimeOn(0);
                appSettings.setGameStatus(true);
                this.recreate();
                break;
            case R.id.buttonRefreshAllMainActivity:
                appSettings.setTime(AppSettings.TIME_FOR_DAY);
                appSettings.setHeartbeats(AppSettings.HEARTBEATS);
                appSettings.setShowTerms(true);
                appSettings.setGameStatus(true);
                appSettings.setCurrentTimeOff(0);
                appSettings.setCurrentTimeOn(0);
                appSettings.setCountUnlockAllTime(0);
                appSettings.setCountUnlockToday(0);
                appSettings.setCountUnlockWeek(0);
                appSettings.setStatisticTimeAllTime(0);
                appSettings.setStatisticTimeToday(0);
                appSettings.setStatisticTimeWeek(0);
                appSettings.setDayOfWeek(0);
                this.recreate();
                break;
        }
    }
}