package com.monks.memento_mori.tools;

import android.app.Activity;
import android.widget.TextView;
import java.util.TimerTask;

public class SetTimeViewWithDelay extends TimerTask {

    private Activity activity;
    private TextView timeView;
    private TimerCountDown timer;

    public SetTimeViewWithDelay(Activity activity, TextView timeView, TimerCountDown timer)
    {
        this.activity = activity;
        this.timeView = timeView;
        this.timer = timer;
    }
    @Override
    public void run()
    {
        activity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                timeView.setText(timer.hms);
            }
        });
    }
}
