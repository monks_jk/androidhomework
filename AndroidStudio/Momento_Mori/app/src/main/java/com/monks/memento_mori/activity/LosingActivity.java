package com.monks.memento_mori.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import com.monks.memento_mori.R;
import com.monks.memento_mori.tools.AppSettings;


public class LosingActivity extends AppCompatActivity implements View.OnClickListener
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_losing);
        Button btnRefresh = (Button) findViewById(R.id.buttonRefreshLosingActivity);
        Button btnNoRefresh = (Button) findViewById(R.id.buttonNoRefreshLosingActivity);
        Button btnRefreshAll = (Button) findViewById(R.id.buttonRefreshAllLosingActivity);
        btnRefresh.setOnClickListener(this);
        btnNoRefresh.setOnClickListener(this);
        btnRefreshAll.setOnClickListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.alfa_exit, R.anim.alfa_enter);
    }

    @Override
    public void onClick(View v) {
        AppSettings appSettings = new AppSettings(this);
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        switch (v.getId()) {
            case R.id.buttonRefreshLosingActivity:
                appSettings.setTime(AppSettings.TIME_FOR_DAY);
                appSettings.setHeartbeats(AppSettings.HEARTBEATS);
                appSettings.setCurrentTimeOff(0);
                appSettings.setCurrentTimeOn(0);
                startActivity(intent);
                finish();
                break;
            case R.id.buttonNoRefreshLosingActivity:
                appSettings.setGameStatus(false);
                startActivity(intent);
                finish();
                break;
            case R.id.buttonRefreshAllLosingActivity:
                appSettings.setShowTerms(true);
                appSettings.setTime(AppSettings.TIME_FOR_DAY);
                appSettings.setHeartbeats(AppSettings.HEARTBEATS);
                appSettings.setCurrentTimeOff(0);
                appSettings.setCurrentTimeOn(0);
                appSettings.setCountUnlockAllTime(0);
                appSettings.setCountUnlockToday(0);
                appSettings.setCountUnlockWeek(0);
                appSettings.setStatisticTimeAllTime(0);
                appSettings.setStatisticTimeToday(0);
                appSettings.setStatisticTimeWeek(0);
                appSettings.setDayOfWeek(0);
                startActivity(intent);
                finish();
                break;
        }
    }
}