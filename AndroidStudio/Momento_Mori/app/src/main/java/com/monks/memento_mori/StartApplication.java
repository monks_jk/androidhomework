package com.monks.memento_mori;

import android.app.Application;

import com.monks.memento_mori.tools.TimerCountDown;
import com.monks.memento_mori.tools.TypefaceUtil;

public class StartApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        TimerCountDown.initInstance(getApplicationContext());
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/font.ttf");
    }
}
