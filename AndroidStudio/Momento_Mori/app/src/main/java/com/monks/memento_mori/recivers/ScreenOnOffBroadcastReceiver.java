package com.monks.memento_mori.recivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.monks.memento_mori.R;
import com.monks.memento_mori.activity.LosingActivity;
import com.monks.memento_mori.activity.VictoryActivity;
import com.monks.memento_mori.tools.AppSettings;

public class ScreenOnOffBroadcastReceiver extends BroadcastReceiver {
    public long currentTimeOff;
    @Override
    public void onReceive(Context context, Intent intent) {
        AppSettings appSettings = new AppSettings(context);
        long currentTimeOn;
        long dissHeart;
        if(intent.getAction().equals(Intent.ACTION_SCREEN_ON)){
            appSettings.setCurrentTimeOn(System.currentTimeMillis() / 1000);
            currentTimeOn = appSettings.getCurrentTimeOn();
            currentTimeOff = appSettings.getCurrentTimeOff();
            dissHeart = appSettings.getHeartbeats() - (currentTimeOn - currentTimeOff);
            if(currentTimeOff != 0){
                if(dissHeart > 0) {
                    appSettings.setHeartbeats(dissHeart);
                    int countUnlockToday = appSettings.getCountUnlockToday();
                    int countUnlockAllTime = appSettings.getCountUnlockAllTime();

                    appSettings.setCountUnlockToday(countUnlockToday + 1);
                    appSettings.setCountUnlockWeek(1);
                    appSettings.setCountUnlockAllTime(countUnlockAllTime + 1);
                }else{
                    notify(context, VictoryActivity.class);//win
                }
            }
        }
        else if(intent.getAction().equals(Intent.ACTION_SCREEN_OFF)){
            appSettings.setCurrentTimeOff(System.currentTimeMillis() / 1000);
            currentTimeOff = appSettings.getCurrentTimeOff();
            currentTimeOn = appSettings.getCurrentTimeOn();
            long dissTime = appSettings.getTime() - (currentTimeOff - currentTimeOn);
            long dissTimeStatistic = currentTimeOff - currentTimeOn;
            if(dissTime > 0){
                appSettings.setTime(dissTime);
                long statisticTimeToday = appSettings.getStatisticTimeToday();
                long statisticTimeAllTime = appSettings.getStatisticTimeAllTime();

                appSettings.setStatisticTimeToday(statisticTimeToday + dissTimeStatistic);
                appSettings.setStatisticTimeWeek(dissTimeStatistic);
                appSettings.setStatisticTimeAllTime(statisticTimeAllTime + dissTimeStatistic);
            }else{
                notify(context, LosingActivity.class);//lose
            }
            dissHeart = appSettings.getHeartbeats() - (currentTimeOff - currentTimeOn);
            if(dissHeart > 0){
                appSettings.setHeartbeats(dissHeart);
            }else{
                notify(context, VictoryActivity.class);//win
            }
        }
    }

    private void notify(Context context,Class<?> cls/*, String title, String text*/){
        /////////////////////////////////////////////////////////////////////////////////////
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_notify)
                        .setContentTitle("My notification")//title
                        .setContentText("Hello World!")//text
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_ALL);
// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, cls);

// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(cls);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
        mNotificationManager.notify(1, mBuilder.build());
////////////////////////////////////////////////////////////////////////////////////////////
    }
}
