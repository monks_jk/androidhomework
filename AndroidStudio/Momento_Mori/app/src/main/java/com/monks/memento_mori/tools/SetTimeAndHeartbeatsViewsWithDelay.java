package com.monks.memento_mori.tools;

import android.app.Activity;
import android.widget.TextView;
import java.util.TimerTask;

public class SetTimeAndHeartbeatsViewsWithDelay extends TimerTask {

    private Activity activity;
    private TextView timeView;
    private TextView heartbeatsView;
    private TimerCountDown timer;

    public SetTimeAndHeartbeatsViewsWithDelay(Activity activity, TextView timeView, TextView heartbeatsView, TimerCountDown timer)
    {
        this.activity = activity;
        this.timeView = timeView;
        this.heartbeatsView = heartbeatsView;
        this.timer = timer;
    }
    @Override
    public void run()
    {
        activity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                timeView.setText(timer.hms);
                heartbeatsView.setText(timer.hmsh);
            }
        });
    }
}
