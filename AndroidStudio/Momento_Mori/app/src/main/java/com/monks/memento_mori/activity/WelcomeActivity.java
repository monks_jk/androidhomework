package com.monks.memento_mori.activity;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.monks.memento_mori.R;
import com.monks.memento_mori.tools.AppSettings;
import com.monks.memento_mori.tools.SetTimeViewWithDelay;
import com.monks.memento_mori.tools.TimerCountDown;

import java.util.Timer;


public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {

    private  Timer timer;
    TimerCountDown timerCountDown = TimerCountDown.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        TextView timeView = (TextView) findViewById(R.id.timeView);

        AppSettings appSettings = new AppSettings(this);
        timerCountDown.startTimer(AppSettings.TIME_FOR_DAY);
        timerCountDown.startTimerHeartbeats(AppSettings.HEARTBEATS);
        appSettings.setCurrentTimeOff(0);
        appSettings.setCurrentTimeOn(0);
        appSettings.setCurrentTimeOn(System.currentTimeMillis() / 1000);
        timer = new Timer();
        SetTimeViewWithDelay task = new SetTimeViewWithDelay(this, timeView, timerCountDown);
        timer.scheduleAtFixedRate(task,TimerCountDown.DELAY,TimerCountDown.PERIOD);
        Button btnGetDate = (Button) findViewById(R.id.buttonWelcomeActivity);
        btnGetDate.setOnClickListener(this);
    }

    @Override
    protected void onPause() {
        timer.cancel();
        timerCountDown.countDownTimer.cancel();
        timerCountDown.countDownTimerHeartbeats.cancel();
        overridePendingTransition(R.anim.alfa_exit, R.anim.alfa_enter);
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonWelcomeActivity:
                setResult(RESULT_OK);
                finish();
                break;
        }
    }
}
