package com.monks.memento_mori.recivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.monks.memento_mori.tools.AppSettings;

public class AlarmBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        AppSettings appSettings = new AppSettings(context);
        if(appSettings.getDayOfWeek() == 6){
            appSettings.setDayOfWeek(0);
        }else{
            appSettings.setDayOfWeek(appSettings.getDayOfWeek() + 1);
        }
        appSettings.setCountUnlockToday(0);
        appSettings.setStatisticTimeToday(0);
        appSettings.setTime(AppSettings.TIME_FOR_DAY);
    }
}

