package com.monks.memento_mori.activity;

import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.monks.memento_mori.R;
import com.monks.memento_mori.tools.AppSettings;
import com.monks.memento_mori.tools.SetTimeAndHeartbeatsViewsWithDelay;
import com.monks.memento_mori.tools.TimerCountDown;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;


public class HistoryActivity extends AppCompatActivity {

    private AppSettings appSettings;
    private TextView timeView;
    private TextView timeViewHearbeats;
    private TimerCountDown timerCountDown = TimerCountDown.getInstance();
    private Timer timer;
    private SetTimeAndHeartbeatsViewsWithDelay task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        appSettings = new AppSettings(this);
        timeView = (TextView) findViewById(R.id.timeViewHistoryActivity);
        timeViewHearbeats = (TextView) findViewById(R.id.heartbeatsViewHistoryActivity);
        ImageView imageViewHeart = (ImageView) findViewById(R.id.imageViewHeartHistoryActivity);
        imageViewHeart.setBackgroundResource(R.drawable.heart_animation);
        setupPieChart();
        AnimationDrawable mAnimationDrawable = (AnimationDrawable) imageViewHeart.getBackground();
        mAnimationDrawable.start();
    }

    @Override
    protected void onResume() {
        if(appSettings.getGameStatus()) {
            calculateWorkTime();
            appSettings.setCurrentTimeOn(System.currentTimeMillis() / 1000);
            timer = new Timer();
            task = new SetTimeAndHeartbeatsViewsWithDelay(this, timeView, timeViewHearbeats, timerCountDown);
            timer.scheduleAtFixedRate(task, TimerCountDown.DELAY, TimerCountDown.PERIOD);
            timerCountDown.startTimer(appSettings.getTime());
            timerCountDown.startTimerHeartbeats(appSettings.getHeartbeats());
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        if(appSettings.getGameStatus()) {
            timer.cancel();
            timerCountDown.countDownTimer.cancel();
            timerCountDown.countDownTimerHeartbeats.cancel();
            overridePendingTransition(R.anim.alfa_exit, R.anim.alfa_enter);
        }
        super.onPause();
    }

    public void setupPieChart() {
        PieChart chart;
        PieDataSet dataSet;
        chart = (PieChart)findViewById(R.id.pieChartHistoryActivity);
        List<PieEntry> pieEntries = new ArrayList<>();
        float a = ((float)appSettings.getHeartbeats()/AppSettings.HEARTBEATS)*100;

        float heartbeatsToWin[] = {100-a, a};
        for (float aHeartbeatsToWin : heartbeatsToWin) {
            pieEntries.add(new PieEntry(aHeartbeatsToWin));
        }
        dataSet = new PieDataSet(pieEntries,null);
        dataSet.setColors(Color.GRAY,Color.BLACK);
        dataSet.setDrawValues(false);
        PieData data = new PieData(dataSet);
        chart.setData(data);
        chart.setTouchEnabled(false);
        chart.setDescription(null);
        chart.setUsePercentValues(false);
        Legend legend = chart.getLegend();
        legend.setEnabled(false);
        chart.invalidate();
    }

    private void calculateWorkTime() {
        appSettings.setCurrentTimeOff(System.currentTimeMillis() / 1000);
        long currentTimeOff = appSettings.getCurrentTimeOff();
        long currentTimeOn = appSettings.getCurrentTimeOn();
        long dissTime = appSettings.getTime() - (currentTimeOff - currentTimeOn);
        long dissTimeStatistic = currentTimeOff - currentTimeOn;
        if (dissTime > 0) {
            appSettings.setTime(dissTime);
            long statisticTimeToday = appSettings.getStatisticTimeToday();
            long statisticTimeAllTime = appSettings.getStatisticTimeAllTime();

            appSettings.setStatisticTimeToday(statisticTimeToday + dissTimeStatistic);
            appSettings.setStatisticTimeWeek(dissTimeStatistic);
            appSettings.setStatisticTimeAllTime(statisticTimeAllTime + dissTimeStatistic);
        }
        long dissHeart = appSettings.getHeartbeats() - (currentTimeOff - currentTimeOn);
        if (dissHeart > 0) {
            appSettings.setHeartbeats(dissHeart);
        }
    }
}
