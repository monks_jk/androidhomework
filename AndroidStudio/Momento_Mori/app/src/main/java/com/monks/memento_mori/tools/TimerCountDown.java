package com.monks.memento_mori.tools;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.monks.memento_mori.R;
import com.monks.memento_mori.activity.LosingActivity;
import com.monks.memento_mori.activity.VictoryActivity;
import com.monks.memento_mori.service.TimeHeartbeatsManagerService;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.concurrent.TimeUnit;


public class TimerCountDown {
    public CountDownTimer  countDownTimer;
    public CountDownTimer  countDownTimerHeartbeats;
    public long timeInSeconds = AppSettings.TIME_FOR_DAY;
    public long timeInSecondsHeartbeats = AppSettings.HEARTBEATS;
    public float pieEntry;
    public String hms;
    public String hmsh;

    private Context context;

    public static long DELAY = 0;
    public static long PERIOD = 1000;

    private static String strange = "#,###";
    private static DecimalFormatSymbols custom = new DecimalFormatSymbols();
    private static DecimalFormat decimal_formatter = new DecimalFormat(strange, custom);
    private static TimerCountDown mInstance;

    public static void initInstance(Context context) {
        if (mInstance == null) {
            mInstance = new TimerCountDown(context);
        }
    }

    public static TimerCountDown getInstance() {
        return mInstance;
    }

    private TimerCountDown (Context context){
        this.context = context;
    }

    public void startTimer(long noOfMinutes) {
        noOfMinutes = noOfMinutes * 1000;
        countDownTimer = new CountDownTimer(noOfMinutes, 1000) {
            public void onTick(long millisUntilFinished) {
                hms = String.format("%02d:%02d:%02d",
                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                timeInSeconds = ((TimeUnit.MILLISECONDS.toHours(millisUntilFinished) * 3600)
                        + ((TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished))) * 60)
                        + (TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }
            public void onFinish() {
                countDownTimerHeartbeats.cancel();
                hms = "Terrible job!";
                hmsh = "Dead!";
                //notify7(context, LosingActivity.class);
                context.stopService(new Intent(context, TimeHeartbeatsManagerService.class));
                Intent intent = new Intent(context, LosingActivity.class);
                context.startActivity(intent);
            }
        }.start();
    }

    public void startTimerHeartbeats(long noOfMinutes) {
        noOfMinutes = noOfMinutes * 1000;
        countDownTimerHeartbeats = new CountDownTimer(noOfMinutes, 1000) {
            public void onTick(long millisUntilFinished) {
                custom.setGroupingSeparator(' ');
                decimal_formatter.setDecimalFormatSymbols(custom);
                decimal_formatter.setGroupingSize(3);
                hmsh = decimal_formatter.format((TimeUnit.MILLISECONDS.toHours(millisUntilFinished)*3600)
                        + ((TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)))*60)
                        + (TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                timeInSecondsHeartbeats = ((TimeUnit.MILLISECONDS.toHours(millisUntilFinished)*3600)
                        + ((TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)))*60)
                        + (TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                pieEntry = ((float)timeInSecondsHeartbeats/AppSettings.HEARTBEATS)*100;
            }
            public void onFinish() {
                hms = "Good job!";
                hmsh = "WIN!";
                countDownTimer.cancel();
                //notify7(context, VictoryActivity.class);
                context.stopService(new Intent(context, TimeHeartbeatsManagerService.class));
                Intent intent = new Intent(context, VictoryActivity.class);
                context.startActivity(intent);
            }
        }.start();
    }

    private static void notify7(Context context, Class<?> cls/*, String title, String text*/){
        /////////////////////////////////////////////////////////////////////////////////////
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_notify)
                        .setContentTitle("My notification")//title
                        .setContentText("Hello World!")//text
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_ALL);
// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, cls);

// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(cls);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
        mNotificationManager.notify(1, mBuilder.build());
////////////////////////////////////////////////////////////////////////////////////////////
    }
}
