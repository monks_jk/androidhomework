package com.monks.l9_example;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView m_textView;
    private EditText m_firstOperandEditText;
    private EditText m_secondOperandEditText;
    float floatFirstOperand = 0, floatSecondOperand = 0, result = 0;
    String strFirstOperand, strSecondOperand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_textView = (TextView) findViewById(R.id.helloWorldTextViewMainActivity);

        Button buttonsSum = (Button)findViewById(R.id.sumButtonMainActivity);
        buttonsSum.setOnClickListener(this);
        Button buttonSub = (Button)findViewById(R.id.subButtonMainActivity);
        buttonSub.setOnClickListener(this);
        Button buttonMul = (Button)findViewById(R.id.mulButtonMainActivity);
        buttonMul.setOnClickListener(this);
        Button buttonDiv = (Button)findViewById(R.id.divButtonMainActivity);
        buttonDiv.setOnClickListener(this);

        m_firstOperandEditText = (EditText) findViewById(R.id.firstOperandEditTextMainActivity);
        m_secondOperandEditText= (EditText) findViewById(R.id.secondOperandEditTextMainActivity);
    }

    @Override
    public void onClick(View view) {
        strFirstOperand = m_firstOperandEditText.getText().toString();
        strSecondOperand = m_secondOperandEditText.getText().toString();
        floatFirstOperand = Float.parseFloat(strFirstOperand);
        floatSecondOperand = Float.parseFloat(strSecondOperand);
        switch (view.getId()){
            case R.id.sumButtonMainActivity:
                result = floatFirstOperand + floatSecondOperand;
                m_textView.setText("Результат: " + result);
                break;
            case R.id.subButtonMainActivity:
                result = floatFirstOperand - floatSecondOperand;
                m_textView.setText("Результат: " + result);
                break;
            case R.id.mulButtonMainActivity:
                result = floatFirstOperand * floatSecondOperand;
                m_textView.setText("Результат: " + result);
                break;
            case R.id.divButtonMainActivity:
                result = floatFirstOperand / floatSecondOperand;
                m_textView.setText("Результат: " + result);
                break;
        }
    }
}
