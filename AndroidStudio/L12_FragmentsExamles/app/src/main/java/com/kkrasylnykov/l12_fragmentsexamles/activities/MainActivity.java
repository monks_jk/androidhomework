package com.kkrasylnykov.l12_fragmentsexamles.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.kkrasylnykov.l12_fragmentsexamles.R;
import com.kkrasylnykov.l12_fragmentsexamles.fragments.FirstFragment;
import com.kkrasylnykov.l12_fragmentsexamles.fragments.SecondFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public FirstFragment firstFragment;
    public SecondFragment secondFragment;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstFragment = new FirstFragment();
        secondFragment = new SecondFragment();
        textView = (TextView) findViewById(R.id.textMainActivity);
        textView.setText("Добавьте фрагмент(ы), и выберите сторону.");
        findViewById(R.id.btnAddFirstMainActivity).setOnClickListener(this);
        findViewById(R.id.btnAddSecondMainActivity).setOnClickListener(this);
        findViewById(R.id.btnAddAllMainActivity).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        switch (view.getId()){
            case R.id.btnAddFirstMainActivity:
                if (firstFragment.isAdded()){
                    transaction.replace(R.id.frameLayout1, firstFragment);
                } else {
                    transaction.add(R.id.frameLayout1, firstFragment);
                }

                break;
            case R.id.btnAddSecondMainActivity:
                if (secondFragment.isAdded()){
                    transaction.replace(R.id.frameLayout2, secondFragment);
                } else {
                    transaction.add(R.id.frameLayout2, secondFragment);
                }
                break;
            case R.id.btnAddAllMainActivity:
                if (firstFragment.isAdded()){
                    transaction.replace(R.id.frameLayout1, firstFragment);
                } else {
                    transaction.add(R.id.frameLayout1, firstFragment);
                }
                if (secondFragment.isAdded()){
                    transaction.replace(R.id.frameLayout2, secondFragment);
                } else {
                    transaction.add(R.id.frameLayout2, secondFragment);
                }
                break;
        }
        transaction.commit();
    }

    public void whatToDoMainActivity(String data, Fragment fragment){
        textView.setText(data);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.remove(fragment);
        transaction.commit();
    }
}
