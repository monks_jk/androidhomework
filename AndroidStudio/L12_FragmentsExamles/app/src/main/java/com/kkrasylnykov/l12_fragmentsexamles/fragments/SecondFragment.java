package com.kkrasylnykov.l12_fragmentsexamles.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l12_fragmentsexamles.R;
import com.kkrasylnykov.l12_fragmentsexamles.activities.MainActivity;

public class SecondFragment extends Fragment implements View.OnClickListener {

    private TextView textView;
    private String strData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second, container, false);
        view.findViewById(R.id.lightSideSecond).setOnClickListener(this);
        view.findViewById(R.id.darkSideSecond).setOnClickListener(this);
        return view;
    }

    public void onClick(View view) {
        Activity activity = getActivity();
        switch (view.getId()){
            case R.id.lightSideSecond:
                if (activity instanceof MainActivity){
                    MainActivity mainActivity = (MainActivity) activity;
                    mainActivity.whatToDoMainActivity("Предатель!!!",mainActivity.secondFragment);
                }
                break;
            case R.id.darkSideSecond:
                if (activity instanceof MainActivity){
                    MainActivity mainActivity = (MainActivity) activity;
                    mainActivity.whatToDoMainActivity("Дарт Вейдер гордился бы тобой!",mainActivity.firstFragment);
                }
                break;
        }
    }
}
