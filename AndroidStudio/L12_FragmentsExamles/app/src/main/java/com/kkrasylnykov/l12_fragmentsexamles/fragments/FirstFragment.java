package com.kkrasylnykov.l12_fragmentsexamles.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kkrasylnykov.l12_fragmentsexamles.R;
import com.kkrasylnykov.l12_fragmentsexamles.activities.MainActivity;

public class FirstFragment extends Fragment implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);
        view.findViewById(R.id.lightSideFirst).setOnClickListener(this);
        view.findViewById(R.id.darkSideFirst).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        Activity activity = getActivity();
        switch (view.getId()){
            case R.id.lightSideFirst:
                if (activity instanceof MainActivity){
                    MainActivity mainActivity = (MainActivity) activity;
                    mainActivity.whatToDoMainActivity("Гордился тобой бы Йода Мастер!",mainActivity.secondFragment);
                }
                break;
            case R.id.darkSideFirst:
                if (activity instanceof MainActivity){
                    MainActivity mainActivity = (MainActivity) activity;
                    mainActivity.whatToDoMainActivity("!!!Предатель",mainActivity.firstFragment);
                }
                break;
        }
    }
}
